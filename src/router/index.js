import Vue from "vue";
import VueRouter from "vue-router";
import VueCookies from 'vue-cookies';

Vue.use(VueRouter);
Vue.use(VueCookies);

const routes = [
  {
    path: "/about",
    name: "About",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: function () {
      return import(/* webpackChunkName: "about" */ "../views/About.vue");
    },
  },
  {
    path: '/loyalty',
    name: 'Loyalty Management Tool',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Loyality.vue')
  },
  {
    path: '/',
    name: 'Monthly Promo Calender',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/MonthlyPromoCal.vue')
  },
  {
    path: '/login',
    name: 'login',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: () => import(/* webpackChunkName: "about" */ '../views/Loginpage.vue')
  },
 
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.name === "login" ) {
    console.log("login page");
    next();
  }
  else{
  if (Vue.$cookies.get("JwtToken") === null){
    //  this.$store.commit('setActivePath',{path: "login"});
    console.log("valid");
     next('/login')
    }
  else{
    // this.$store.commit('setActivePath',{path: to.name});
    console.log("invalid");
    next()
  }
}
});


export default router;
