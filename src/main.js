import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import vuetify from "./plugins/vuetify";
import JsonExcel from "vue-json-excel";
import VueCookies from 'vue-cookies';

Vue.component("downloadExcel", JsonExcel);
Vue.config.productionTip = false;
Vue.use(VueCookies);

new Vue({
  router,
  store,
  vuetify,

  render: function (h) {
    return h(App);
  },
}).$mount("#app");
