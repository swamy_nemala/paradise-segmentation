import Vue from "vue";
import Vuex from "vuex";
import VueCookies from 'vue-cookies';

Vue.use(Vuex);
Vue.use(VueCookies);

export default new Vuex.Store({
  state: {
    mini:true,
    islogin:false,
    JWTtoken:"",
    activePath: "",
    page_name: {
      name: "",
    },
    
  },
  mutations: {
    updatemini:(state,payload)=>{
      
      state.mini = payload;
      console.log("index changed",state.mini);
    },
    updatelogin:(state,payload)=>{
      
      state.islogin = payload;
      console.log("index changed",state.islogin);
    },
    updateJwt:(state,payload)=>{
      
      state.JWTtoken = payload;
      console.log("jwt changed",state.JWTtoken);
      state.islogin = true;
    },
    pagename: (state, payload) => {

      state.page_name.name = payload;
    },
  },
  actions: {
    updatemini: (context, payload) => {
      setTimeout(function () {
        context.commit('updatemini', payload);
      }, 100)
    },
    updatelogin: (context, payload) => {
      setTimeout(function () {
        context.commit('updatelogin', payload);
      }, 100)
    },
    pagename: (context, payload) => {
      setTimeout(function () {
        context.commit('pagename', payload);
      }, 100)
    },
  },
  modules: {},
});
